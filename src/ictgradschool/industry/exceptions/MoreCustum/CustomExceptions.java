package ictgradschool.industry.exceptions.MoreCustum;

import ictgradschool.Keyboard;

public class CustomExceptions {
    public void start() {
        String theWord = getUserWord();

        try {
            printUserInformation(theWord);
        } catch (InvalidWordException | ExceedMaxStringLengthException e) {
            System.out.println("You have thrown a error!!");
        }

    }

    private String getUserWord() {
        System.out.println("Enter a string of at most 100 characters: ");
        String theWord = Keyboard.readInput();
        return theWord;
    }

    private void printUserInformation(String theWord) throws InvalidWordException, ExceedMaxStringLengthException {
        String[] wordArray = theWord.split(" ");
        String myString = "";
        for (int i = 0; i < wordArray.length; i++) {
            String letterWhat = wordArray[i];
            char chosenOnes = letterWhat.charAt(0);
            if (Character.isDigit(chosenOnes)) {
                throw new InvalidWordException();
            } else if (theWord.length() > 100)
                throw new ExceedMaxStringLengthException();
            else {
                myString += (chosenOnes + " ");
            }
        }
        System.out.println("You entered: " + myString);
    }

    public static void main(String[] args) {
        CustomExceptions c = new CustomExceptions();
        c.start();

    }
}
